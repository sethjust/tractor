.PHONY: all deps build test run deploy clean reallyclean dockershell

SRCS = src/Main.purs src/Tractor.purs

TESTS = test/Main.purs

all: deps build test

deps: bower.json
	bower install
	
build: deps $(SRCS)
	pulp build

test: build $(TESTS)
	pulp test

run: build
	pulp run

deploy: public/main.js

public/main.js: build
	pulp build --optimise --to public/main.js

clean:
	rm -rf output

reallyclean: clean
	rm -rf .pulp-cache bower_components 

dockershell:
	docker run -it -v `pwd`:/home/pureuser/tractor/ fretlink/purescript-docker:0.12.0 /home/pureuser/tractor/dockershell.sh
