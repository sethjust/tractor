#!/bin/bash

# This script is used to set up a shell
# in a docker container. Instead of running
# this script, instead run
#
#    make dockershell

cd /home/pureuser/tractor
exec bash
