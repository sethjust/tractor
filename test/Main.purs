module Test.Main where

import Prelude

import Data.Int (round)
import Data.List.Lazy ((!!))
import Data.Maybe (Maybe(..), maybe)
import Data.Number (isFinite, isNaN, nan)
import Data.Number.Format (toStringWith, precision)

import Effect (Effect)
import Test.Spec (describe, pending, pending', it)
import Test.Spec.Assertions (shouldEqual)
import Test.Spec.QuickCheck (quickCheck)
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (run)

import Test.QuickCheck ((===), (/==), (>?), (>=?), (<=?), (<?>), Result(..))
import Test.QuickCheck.Arbitrary
import Test.QuickCheck.Gen (uniform)

import Units
import Tractor

newtype ArbOmega = ArbOmega Omega
instance arbitraryArbOmega :: Arbitrary ArbOmega where
  arbitrary = map (\o -> ArbOmega $ Omega $ o * 100.0) uniform

newtype ArbTractor = ArbTractor Tractor
instance arbitraryArbTractor :: Arbitrary ArbTractor where
  arbitrary = do
    m <- uniform
    r <- uniform
    mt' <- uniform
    j <- uniform
    let 
      mt = Torque $ mt'*mt'*400.0+20.0 -- must be large enough to overcome rolling resistance!
    pure $ ArbTractor {
      mass: Mass $ m*2000.0+100.0
      ,wheelRadius: Length $ r/2.0+0.1
      ,maxTorque: TorqueCurve {
        name: "Constant torque curve of " <> show mt
        ,maxTorque: \_ -> mt
      }
      ,mu: stdMu
      ,j: Moment $ j*3.0+0.1
    }

newtype ArbTractionState = ArbTractionState TractionState
instance arbitraryArbTractionState :: Arbitrary ArbTractionState where
  arbitrary = do
    (ArbTractor t@{wheelRadius: r}) <- arbitrary
    u <- uniform
    (ArbOmega o) <- arbitrary
    pure $ ArbTractionState {
      tractor: t
      ,speed: Velocity $ min (u * 10.0) (fromOmega o * fromLength r)
      ,omega: o
    }

newtype ArbDragState = ArbDragState DragState
instance arbitraryArbDragState :: Arbitrary ArbDragState where
  arbitrary = do
    (ArbTractionState tst) <- arbitrary
    t <- arbitrary
    x <- arbitrary
    pure $ ArbDragState {
      tractionState: tst
      ,time: Time t
      ,position: Length x
    }

delta' d a b | a > b = not isNaN e && d > e where e = a - b
             | otherwise = not isNaN e && d > e where e = b - a

delta d a b | a > b && isNaN (a - b) = Failed "delta was nan"
            | a > b = d >? a - b
            | isNaN (b - a) = Failed "delta was nan"
            | otherwise = d >? b - a

deltaTime = Time 0.05
fn = stepTime deltaTime
fn' = stepTime' deltaTime

totE ts =
  k + r
  where 
    ke m u = 0.5 * m * u * u
    re j omega = 0.5 * j * omega * omega

    k = ke (fromMass ts.tractor.mass) (fromVelocity ts.speed)
    r = re (fromMoment ts.tractor.j) (fromOmega ts.omega)

desc :: DragState -> String
desc ds = (show ds)
  <> " (slip="
  <> show (slip ds.tractionState)
  <> ", accel="
  <> show (accel ds.tractionState)
  <> ", drag="
  <> show (drag ds.tractionState)
  <> ", totE="
  <> (toStringWith $ precision 5) (totE ds.tractionState)
  <> ")"

failure (dst :: DragState) (nst :: DragState) = "failed for:\n"
  <> (desc dst)
  <> "\n"
  <> (desc nst)

main :: Effect Unit
main = run [consoleReporter] do
  describe "delta'" do
    it "fails for NaN" $
      quickCheck \d -> not (1.0 `delta' d` nan) <?> "got true"
  describe "delta" do
    it "fails for NaN" $
      quickCheck \d ->
        case (1.0 `delta d` nan) of
          Success -> Failed "got Success"
          (Failed msg) -> Success
  describe "slip" do
    it "returns -1 for pure slip (u=0)" $
      quickCheck \(ArbTractionState st) -> (-1.0) `delta 1e-6` (fromSlip $ slip (st {speed = Velocity 0.0}))
    it "returns -1 for pure slip (u=0) at omega=0" $
      quickCheck \(ArbTractionState st) -> (-1.0) `delta 1e-6` (fromSlip $ slip (st {speed = Velocity 0.0, omega = Omega 0.0}))
    it "returns 0 for u = omega r > 0" $
      quickCheck \(ArbTractionState st) -> 0.0 `delta 1e-6` (fromSlip $ slip (st {speed = Velocity (fromOmega st.omega * fromLength st.tractor.wheelRadius)}))
    it "is not less than -1" $
      quickCheck \(ArbTractionState st) -> (-1.0) <=? (fromSlip $ slip st)
    it "is not more than zero" $
      quickCheck \(ArbTractionState st) -> (0.0) >=? (fromSlip $ slip st)
    it "is sensible for initialState" $
      quickCheck \(ArbTractor t) ->
        let
          s = fromSlip $ slip (initialState t).tractionState
        in 
          (isFinite s) <?> ("bad slip: " <> (show s))
  describe "traction" do
    it "returns maxTorque" $
      quickCheck \(ArbTractionState st@{tractor: {maxTorque: TorqueCurve {maxTorque: mt}}, omega: o}) -> traction st === mt o
  --describe "rkAdaptive'" do
  --  it ""
  describe "stepTime'" do
    it "returns a greater speed if slip is nonzero and maxTorque is nonzero" $ 
      quickCheck \(ArbDragState dst') -> let
          dst = dst' { tractionState {
            tractor {
              maxTorque = let t = Torque 100.0 
                          in  TorqueCurve {name: "constant torque of " <> (show t), maxTorque: \_ -> t }
            }
            -- ensure that there's some degree of slip
            ,speed = Velocity $ 0.95 * (fromOmega dst'.tractionState.omega * fromLength dst'.tractionState.tractor.wheelRadius)
          } }
          step = fn' dst 
        in
          fromVelocity step.u > (fromVelocity dst.tractionState.speed) * 0.999
          <?> (show dst <> "\n" <> show step)
  describe "stepTime" do
    it "doesn't alter tractor" $
      quickCheck \(ArbDragState dst) -> (fn dst).tractionState.tractor.wheelRadius === dst.tractionState.tractor.wheelRadius --TODO: don't only compare radius
    it "returns a strictly greater time" $
      quickCheck \(ArbDragState dst) -> (fn dst).time >? dst.time
    it "returns a non-decreasing position" $
      quickCheck \(ArbDragState dst) -> (fn dst).position >=? dst.position
    it "returns no higher slip if maxTorque is always zero" $
      quickCheck \(ArbDragState dst') -> let
          dst = dst' { tractionState { tractor { maxTorque = TorqueCurve {name: "zero torque", maxTorque: \_ -> Torque 0.0 } } } }
          nst = fn $ dst
          (Slip ds) = slip dst.tractionState
          (Slip ns) = slip nst.tractionState
        in
          (0.0 == ns || ns > ds || ns `delta' 1e-5` ds)
          <?> failure dst nst
    it "returns lower or zero speed if maxTorque is always zero and slip is near zero" $
      quickCheck \(ArbDragState dst') -> let
          dst = dst' { tractionState {
            tractor {
              maxTorque = let t = Torque 0.0 
                          in  TorqueCurve {name: "zero torque", maxTorque: \_ -> t }
            }
            -- ensure that there's very little slip
            ,speed = Velocity $ 0.999 * (fromOmega dst'.tractionState.omega * fromLength dst'.tractionState.tractor.wheelRadius)
          } }
          nst = fn $ dst
          (Velocity ns) = nst.tractionState.speed
        in
          (0.0 == ns || ns < fromVelocity dst.tractionState.speed)
          <?> failure dst nst
    it "at zero slip and nonzero max torque it sees increased slip and speed in a single step" $
      -- this can fail if the torque is too low to overcome rolling resistance
      quickCheck \(ArbDragState dst') ->
        let
          -- enforce zero slip
          dst = dst' {tractionState {speed = Velocity (fromOmega dst'.tractionState.omega * fromLength dst'.tractionState.tractor.wheelRadius)} }
          nst = fn $ dst
          os = fromSlip $ slip dst.tractionState
          ns = fromSlip $ slip nst.tractionState
          ou = fromVelocity dst.tractionState.speed
          nu = fromVelocity nst.tractionState.speed
        in
          ((0.0 `delta' 1e-6` os) && (os >= ns || (ns `delta' 1e-6` 0.0)) && (ou <= nu))
          <?> failure dst nst
    it "doesn't exceed the available energy budget" $
      quickCheck \(ArbDragState dst') ->
        let
          dst = dst' { tractionState {
            tractor {
              maxTorque = let t = Torque 0.0 
                          in  TorqueCurve {name: "zero torque", maxTorque: \_ -> t }
            }
          }}

          nst = fn dst

          oe = totE dst.tractionState
          ne = totE nst.tractionState
        in
          oe > ne
          <?> failure dst nst
  describe "simulate" do
    it "doesn't just give NaN" $
      quickCheck \(ArbTractor t) ->
        let
          ls = simulate deltaTime t
          nst' = ls !! 1
          assrt nst = (not (isNaN $ fromSlip (slip nst.tractionState))) <?> ("Slip was " <> show (slip nst.tractionState) <> ": " <> (show nst))
        in
          maybe (false <?> "No such element in returned list") assrt nst'
    pending' "results in a massive burnout 🏎☁️☁️" $
      quickCheck \(ArbTractor t) ->
        let
          nst' = simulate deltaTime t !! (round (4.0 / 0.05)) -- four seconds
          assrt nst = (fromSlip (slip nst.tractionState)) <=? -0.8
        in
          maybe (false <?> "No such element in returned list") assrt nst'
          
