module Tractor where

import Prelude

import Math (abs, exp)

import Data.List.Lazy (List(..), iterate, takeWhile)

import Units

--| returns dimensionless friction coefficient
newtype Mu = Mu {
  mu:: Slip -> Number
  ,name:: String
}

instance showMu :: Show Mu where
  show (Mu {name: n}) = n

stdMu :: Mu
stdMu = let
   c1 = 1.18
   c2 = 10.0
   c3 = 0.5
 in Mu {
    name: "Standard Traction Characteristic"
    ,mu: \(Slip s) -> c1 * (1.0 - (exp $ c2 * s)) + c3 * s
  }

newtype TorqueCurve = TorqueCurve {
  maxTorque :: Omega -> Torque
  ,name :: String
}

instance showTorqueCurve :: Show TorqueCurve where
  show (TorqueCurve {name: n}) = n

type Tractor = {
  --| The maximum torque that this tractor
  --| can extert at a given angular velocity
  mass :: Mass
  ,maxTorque :: TorqueCurve
  ,wheelRadius :: Length
  ,mu :: Mu
  ,j :: Moment
}

mu :: Tractor -> Slip -> Number
mu {mu: Mu {mu: fn}} = fn

--| `TractionState` represents the state of a tractor at a point in time.
type TractionState = {
  tractor :: Tractor
  ,speed :: Velocity
  ,omega :: Omega
}

slip :: TractionState -> Slip
slip { speed: Velocity u, omega: Omega o, tractor: {wheelRadius: Length r} } | u == 0.0  = Slip $ -1.0
                                                                             | otherwise = Slip $ (u-o*r)/o/r

--| `traction` computes the optimal torque for a tractor at a point
--| in time.
--|
--| It's meant to be used in advancing a simulation.
--|
--| (For now) simply returns the maximum torque available.
traction :: TractionState -> Torque
traction {tractor: {maxTorque: TorqueCurve {maxTorque: mt}}, omega: o} = mt o

type DragState = {
  tractionState :: TractionState
  ,time :: Time
  ,position :: Length
}

--| `initialState'` gives the given tractor at a standstill
--| at zero time and distance.
initialState t = {
    tractionState: {
      tractor: t
      ,speed: Velocity 0.0
      ,omega: Omega 0.0
    }
    ,time: Time 0.0
    ,position: Length 0.0
  }

--| `simulate` just returns a list of states
simulate :: Time -> Tractor -> List DragState
simulate dt tractor = 
  let
    doStep {state: state, step: step'} =
      let
        step = stepTime' step'.dt state
      in
        {
          state: applyStep state step
          ,step: step
        }
    results = doStep `iterate` {state: initialState tractor, step: {dt: dt, dx: Length 0.0, u: Velocity 0.0, o: Omega 0.0, ex: 0.0, eu: 0.0, es: 0.0}}
  in
    map _.state results

--| `simulateRace` just returns a list of states
--| with positions at or before the finish line
--|
--| TODO: consider a stateful `takeWhile` that takes
--| the first point after the finish as well
simulateRace :: Time -> Length -> Tractor -> List DragState
simulateRace dt (Length l) tractor = takeWhile (\{position: Length x} -> l >= x) (simulate dt tractor)

applyStep :: DragState -> Step -> DragState
applyStep state step = state {
    tractionState {
      speed = step.u
      ,omega = step.o
    }
    ,time = state.time + step.dt
    ,position = state.position + step.dx
  }

--| takes a time step and applies it to a state
stepTime :: Time -> DragState -> DragState
stepTime timeStep state =
  let
    step = stepTime' timeStep state
  in
    applyStep state step

type Step = {dt :: Time, dx :: Length, u :: Velocity, o :: Omega, ex :: Number, eu :: Number, es :: Number}

stepTime' :: Time -> DragState -> Step
stepTime' = stepTimeRk

--| For a given state, compute acceleration due to tire forces.
--| This value is used in modeling tire slip, so it should consider
--| only torques reaching the wheel via the hub; rolling resistance
--| and drag will be computed separately.
accel :: TractionState -> Accel
accel ts = Accel $ (fromAccel g) * (mu ts.tractor (slip ts))

--| For numerical simulation
accelN :: TractionState -> Number
accelN ts = fromAccel $ accel ts

--| Coefficient of rolling resistance
coRollRes :: Tractor -> Velocity -> Number
coRollRes _ _ = 0.012

--| For a given state, compute acceleration due to rolling resistance.
--| This value should have a sign indicating acceleration in
--| the same direction as `accel` (so it will always be negative).
--|
--| As a force (in `N`) this is just `crr * g * m` where `m` is the
--| mass of the tractor, but this is cancelled out when calculating
--| acceleration.
rollRes :: TractionState -> Accel
rollRes ts = Accel $ -1.0 * (coRollRes ts.tractor ts.speed) * (fromAccel g)

--| For a given state, map speed to acceleration due to drag and
--| rolling resistance. This value should have a sign indicating
--| acceleration in the same direction as `accel` (so it will
--| always be negative).
drag :: TractionState -> Accel
drag = rollRes

--| For numerical simulation
dragN :: TractionState -> Number
dragN ts = fromAccel $ drag ts

--| returns (time step, distance step, final speed, final omega)
stepTimeRk :: Time -> DragState -> Step
stepTimeRk (Time dt) st =
  let
    -- rk4 for three ODEs
    -- x' = f(t,x,u,s)
    -- u' = g(t,x,u,s)
    -- s' = h(t,x,u,s)
    rk4 d f g h t0 x0 u0 s0 = {
        t: t3
        ,x: x0 + (j0 + 2.0 * j1 + 2.0 * j2 + j3) / 6.0
        ,u: u0 + (k0 + 2.0 * k1 + 2.0 * k2 + k3) / 6.0
        ,s: s0 + (l0 + 2.0 * l1 + 2.0 * l2 + l3) / 6.0
      } where
        j0 = d * f t0 x0 u0 s0
        k0 = d * g t0 x0 u0 s0
        l0 = d * h t0 x0 u0 s0
        t1 = t0 + d / 2.0
        x1 = x0 + j0 / 2.0
        u1 = u0 + k0 / 2.0
        s1 = s0 + l0 / 2.0
        j1 = d * f t1 x1 u1 s1
        k1 = d * g t1 x1 u1 s1
        l1 = d * h t1 x1 u1 s1
        x2 = x0 + j0 / 2.0
        u2 = u0 + k0 / 2.0
        s2 = s0 + l0 / 2.0
        j2 = d * f t1 x2 u2 s2
        k2 = d * g t1 x2 u2 s2
        l2 = d * h t1 x2 u2 s2
        t3 = t0 + d
        x3 = x0 + j2
        u3 = u0 + k2
        s3 = s0 + l2
        j3 = d * f t3 x3 u3 s3
        k3 = d * g t3 x3 u3 s3
        l3 = d * h t3 x3 u3 s3

    -- a couple helpful defs
    toOmega r u s = Omega $ u / (r * s + r)
    {wheelRadius: Length r} = st.tractionState.tractor

    (Length x0) = st.position
    (Time t0) = st.time
    (Velocity u0) = st.tractionState.speed
    (Slip s0) = slip st.tractionState

    maxSlip = -0.995
    minU = 0.001

    -- equations of motion
    x' t x u s = u

    u' t x u s | u < 0.0 = u' t x 0.0 s
               | s > 0.0 = u' t x u 0.0
               | s < maxSlip = u' x t u maxSlip
               | otherwise = let ts = st.tractionState { speed = Velocity u, omega = o }
                             in  (accelN ts) + (dragN ts) -- plus, as signs are in the same direction
      where
        o = toOmega r u s

    s' t x u s | s > 0.0 = s' t x u 0.0
               | s < maxSlip = s' t x u maxSlip
               | u < minU = s' t x minU s
               | otherwise = (fromAccel g) * (h_t s) / u
      where
        (Torque torque) = traction $ st.tractionState {speed = Velocity u, omega = toOmega r u s}

        {mass: Mass m, wheelRadius: Length r, mu: Mu {mu: mu}, j: Moment j} = st.tractionState.tractor

        phi = m * r * r / j
        gamma = torque * r / (j * (fromAccel g))

        h_t s = (s + 1.0) * (s + 1.0) * (((1.0 + phi) * mu(Slip s) - gamma)/(s+1.0))

    -- rk45 for three ODEs, constrained to expected values
    -- TODO: instead of constraining, force step size reduction when OOB
    -- 
    -- x' = f = u
    -- u' = g = u'
    -- s' = h = s'
    rk45constr d t0 x0 u0 s0 = { t: t6, x: x6, u: u6, s: s6, ex: ex, eu: eu, es: es }
      where
        f = x'
        g = u'
        h = s'

        lin o a0 m0 a1 m1 a2 m2 a3 m3 a4 m4 a5 m5 = o + a0 * m0 + a1 * m1 + a2 * m2 + a3 * m3 + a4 * m4 + a5 * m5

        -- helpers for constrained extrapolation
        _f = lin x0

        _g a0 m0 a1 m1 a2 m2 a3 m3 a4 m4 a5 m5 | lin u0 a0 m0 a1 m1 a2 m2 a3 m3 a4 m4 a5 m5 < 0.0 = 0.0
                                               | otherwise                                        = lin u0 a0 m0 a1 m1 a2 m2 a3 m3 a4 m4 a5 m5

        _h a0 m0 a1 m1 a2 m2 a3 m3 a4 m4 a5 m5 | lin s0 a0 m0 a1 m1 a2 m2 a3 m3 a4 m4 a5 m5 > 0.0     = 0.0
                                               | lin s0 a0 m0 a1 m1 a2 m2 a3 m3 a4 m4 a5 m5 < maxSlip = maxSlip
                                               | otherwise                                            = lin s0 a0 m0 a1 m1 a2 m2 a3 m3 a4 m4 a5 m5

        -- carry out RK
        j0 = d * f t0 x0 u0 s0
        k0 = d * g t0 x0 u0 s0
        l0 = d * h t0 x0 u0 s0

        t1 = t0 + d * 0.25
        x1 = _f 0.25 j0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0
        u1 = _g 0.25 k0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0
        s1 = _h 0.25 l0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0

        j1 = d * f t1 x1 u1 s1
        k1 = d * g t1 x1 u1 s1
        l1 = d * h t1 x1 u1 s1

        t2 = t0 + d * (3.0 / 8.0)
        x2 = _f (3.0 / 32.0) j0 (9.0 / 32.0) j1 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0
        u2 = _g (3.0 / 32.0) k0 (9.0 / 32.0) k1 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0
        s2 = _h (3.0 / 32.0) l0 (9.0 / 32.0) l1 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0

        j2 = d * f t2 x2 u2 s2
        k2 = d * g t2 x2 u2 s2
        l2 = d * h t2 x2 u2 s2

        t3 = t0 + d * (12.0 / 13.0)
        x3 = _f (1932.0 / 2197.0) j0 (-7200.0 / 2197.0) j1 (7296.0 / 2197.0) j2 0.0 0.0 0.0 0.0 0.0 0.0
        u3 = _g (1932.0 / 2197.0) k0 (-7200.0 / 2197.0) k1 (7296.0 / 2197.0) k2 0.0 0.0 0.0 0.0 0.0 0.0
        s3 = _h (1932.0 / 2197.0) l0 (-7200.0 / 2197.0) l1 (7296.0 / 2197.0) l2 0.0 0.0 0.0 0.0 0.0 0.0

        j3 = d * f t3 x3 u3 s3
        k3 = d * g t3 x3 u3 s3
        l3 = d * h t3 x3 u3 s3

        t4 = t0 + d * (1.0)
        x4 = _f (439.0 / 216.0) j0 (-8.0) j1 (3680.0 / 513.0) j2 (-845.0 / 4104.0) j3 0.0 0.0 0.0 0.0
        u4 = _g (439.0 / 216.0) k0 (-8.0) k1 (3680.0 / 513.0) k2 (-845.0 / 4104.0) k3 0.0 0.0 0.0 0.0
        s4 = _h (439.0 / 216.0) l0 (-8.0) l1 (3680.0 / 513.0) l2 (-845.0 / 4104.0) l3 0.0 0.0 0.0 0.0

        j4 = d * f t4 x4 u4 s4
        k4 = d * g t4 x4 u4 s4
        l4 = d * h t4 x4 u4 s4

        t5 = t0 + d * 0.5
        x5 = _f (-8.0 / 27.0) j0 (2.0) j1 (-3544.0 / 2565.0) j2 (1859.0 / 4104.0) j3 (-11.0 / 40.0) j4 0.0 0.0
        u5 = _g (-8.0 / 27.0) k0 (2.0) k1 (-3544.0 / 2565.0) k2 (1859.0 / 4104.0) k3 (-11.0 / 40.0) k4 0.0 0.0
        s5 = _h (-8.0 / 27.0) l0 (2.0) l1 (-3544.0 / 2565.0) l2 (1859.0 / 4104.0) l3 (-11.0 / 40.0) l4 0.0 0.0

        j5 = d * f t5 x5 u5 s5
        k5 = d * g t5 x5 u5 s5
        l5 = d * h t5 x5 u5 s5

        t6 = t0 + d
        x6 = _f (25.0 / 216.0) j0 0.0 j1 (1408.0 / 2565.0) j2 (2197.0 / 4104.0) j3 (-1.0 / 5.0) j4 0.0 j5
        u6 = _g (25.0 / 216.0) k0 0.0 k1 (1408.0 / 2565.0) k2 (2197.0 / 4104.0) k3 (-1.0 / 5.0) j4 0.0 j5
        s6 = _h (25.0 / 216.0) l0 0.0 l1 (1408.0 / 2565.0) l2 (2197.0 / 4104.0) l3 (-1.0 / 5.0) j4 0.0 j5
        
        x7 = _f (16.0 / 135.0) j0 0.0 j1 (6656.0 / 12825.0) j2 (28561.0 / 56430.0) j3 (-9.0 / 50.0) j4 (2.0 / 55.0) j5
        u7 = _g (16.0 / 135.0) k0 0.0 k1 (6656.0 / 12825.0) k2 (28561.0 / 56430.0) k3 (-9.0 / 50.0) j4 (2.0 / 55.0) j5
        s7 = _h (16.0 / 135.0) l0 0.0 l1 (6656.0 / 12825.0) l2 (28561.0 / 56430.0) l3 (-9.0 / 50.0) j4 (2.0 / 55.0) j5

        ex = abs $ x7 - x6
        eu = abs $ u7 - u6
        es = abs $ s7 - s6

    rkAdaptive integ d t0 x0 u0 s0 = rkAdaptive' integ d t0 x0 u0 s0 $ integ d t0 x0 u0 s0

    rkAdaptive' integ d t0 x0 u0 s0 step@{t: t6, x: x6, u: u6, s: s6, ex: ex, eu: eu, es: es}
      | d < 1.0 && ex < 0.001 && eu < 0.001 && es < 0.0001 = rkAdaptive integ (2.0 * d) t0 x0 u0 s0
      | d > 0.001 && (ex > 0.01 || eu > 0.01 || es > 0.0005) = rkAdaptive integ (0.2 * d) t0 x0 u0 s0
      | otherwise = step

    -- perform integration
    res@{t: t1, x: x1, u: u1, s: s1} = rkAdaptive rk45constr dt t0 x0 u0 s0

    d = t1 - t0
    dx = x1 - x0

    -- compute final omega
    o1 = toOmega r u1 s1
  in {
    dt: Time d
    ,dx: Length dx
    ,u: Velocity u1
    ,o: o1
    ,ex: res.ex
    ,eu: res.eu
    ,es: res.es
  }
