module Main where

import Prelude
import Effect (Effect, foreachE)
import Effect.Console (logShow)

import Partial.Unsafe (unsafePartial)

import Data.Array (fromFoldable)
import Data.Foldable (foldl)
import Data.List.Lazy (take)
import Data.Maybe (Maybe(..), fromJust)
import Data.Number.Format (toStringWith, fixed)

import Web.DOM.Document (createElement, createTextNode)
import Web.DOM.Element (setAttribute, setClassName, toNode)
import Web.DOM.Node (appendChild)
import Web.DOM.ParentNode (QuerySelector(..), querySelector)
import Web.DOM.Text (toNode) as Text
import Web.HTML (window)
import Web.HTML.HTMLDocument (body, toDocument)
import Web.HTML.HTMLElement (toParentNode)
import Web.HTML.Window (document)

import Units
import Tractor

main :: Effect Unit
main =
  let
    demoTractor = {
        mass: Mass 1220.0
        ,wheelRadius: Length 0.2
        ,maxTorque:
          let
            mt = Torque 190.0
          in TorqueCurve {
            name: "Constant torque curve of " <> show mt
            ,maxTorque: \_ -> mt
          }
        ,mu: stdMu
        ,j: Moment $ 1.0
      }

    n = 100
    demo = take n $ simulate (Time 0.075) demoTractor

    maxU = foldl (\u {tractionState: {speed: Velocity _u}} -> max u _u) 0.0 demo
    maxT = foldl (\t {time: Time _t} -> max t _t) 0.0 demo

  in do
    doc <- window >>= document
    b <- body doc

    qch <- querySelector (QuerySelector "#chart") (toParentNode $ unsafePartial $ fromJust b)

    let ch = unsafePartial $ fromJust qch

    let df = toStringWith (fixed 4)

    _ <- setAttribute "style" ("--data-max-time:" <> (df maxT) <> "; --data-max-value:" <> (df maxU) <> ";") ch

    foreachE (fromFoldable demo) \st ->
      let
        repr {time: t, tractionState: st@{speed: u}} d = do
          let ts = (fromTime >>> toStringWith (fixed 2)) t
          let us = (fromVelocity >>> toStringWith (fixed 2)) u
          let ss = (fromSlip >>> toStringWith (fixed 3)) (slip st)
          let tt = ts <> "s " <> us <> "m/s slip: " <> ss

          let td = (fromTime >>> df) t
          let ud = (fromVelocity >>> df) u

          e <- createElement "div" (toDocument doc) --TODO: use <li>
          s <- createElement "span" (toDocument doc)
          n <- createTextNode tt d
          _ <- appendChild (toNode s) (toNode e)
          _ <- appendChild (Text.toNode n) (toNode s)
          _ <- setClassName ("speed point" ) $ e
          _ <- setAttribute "style" ("--data-value:" <> ud <> "; --data-time:" <> td <> ";") e
          pure e
      in do
        e <- repr st (toDocument doc)
        _ <- appendChild (toNode e) (toNode ch)
        pure unit
