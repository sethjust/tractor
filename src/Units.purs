module Units where

import Prelude
import Data.Number.Format (toStringWith, precision)

format :: Number -> String
format = toStringWith (precision 5)


--| an angular velocity in `rad s**-1`
newtype Omega = Omega Number

fromOmega :: Omega -> Number
fromOmega (Omega n) = n

instance showOmega :: Show Omega where
  show (Omega n) = format n <> " rad s**-1"

derive newtype instance eqOmega :: Eq Omega
derive newtype instance ordOmega :: Ord Omega

--| `N m`
newtype Torque = Torque Number

fromTorque :: Torque -> Number
fromTorque (Torque n) = n

instance showTorque :: Show Torque where
  show (Torque n) = format n <> " N m"

derive newtype instance eqTorque :: Eq Torque
derive newtype instance ordTorque :: Ord Torque

--| `m`
newtype Length = Length Number

fromLength :: Length -> Number
fromLength (Length l) = l

instance showLength :: Show Length where
  show (Length n) = format n <> " m"

derive newtype instance eqLength :: Eq Length
derive newtype instance ordLength :: Ord Length

--| TODO: instead use custom typeclasses that provide implementations for Number but don't require being a Semiring
derive newtype instance semiringLength :: Semiring Length

--| `m s**-1`
newtype Velocity = Velocity Number

fromVelocity :: Velocity -> Number
fromVelocity (Velocity n) = n

instance showVelocity :: Show Velocity where
  show (Velocity n) = format n <> " m s**-1"

derive newtype instance eqVelocity :: Eq Velocity
derive newtype instance ordVelocity :: Ord Velocity

--| `m s**-2`
newtype Accel = Accel Number

fromAccel :: Accel -> Number
fromAccel (Accel n) = n

instance showAccel :: Show Accel where
  show (Accel n) = format n <> " m s**-1"

derive newtype instance eqAccel :: Eq Accel
derive newtype instance ordAccel :: Ord Accel

g :: Accel
g = Accel 9.80665

newtype Slip = Slip Number

fromSlip :: Slip -> Number
fromSlip (Slip s) = s

instance showSlip :: Show Slip where
  show (Slip n) = format n

derive newtype instance eqSlip :: Eq Slip
derive newtype instance ordSlip :: Ord Slip

--| in `sec`
newtype Time = Time Number

fromTime :: Time -> Number
fromTime (Time t) = t

instance showTime :: Show Time where
  show (Time t) = format t <> " s"

derive newtype instance eqTime :: Eq Time
derive newtype instance ordTime :: Ord Time

--| TODO: instead use custom typeclasses that provide implementations for Number but don't require being a Semiring
derive newtype instance semiringTime :: Semiring Time

--| in `kg`
newtype Mass = Mass Number

fromMass :: Mass -> Number
fromMass (Mass m) = m

instance showMass :: Show Mass where
  show (Mass m) = format m <> " kg"

derive newtype instance eqMass :: Eq Mass
derive newtype instance ordMass :: Ord Mass

--| in `kg m**2`
newtype Moment = Moment Number

fromMoment :: Moment -> Number
fromMoment (Moment j) = j

instance showMoment :: Show Moment where
  show (Moment j) = format j <> " kg m**2"

derive newtype instance eqMoment :: Eq Moment
derive newtype instance ordMoment :: Ord Moment
